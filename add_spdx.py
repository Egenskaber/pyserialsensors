# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

#!/bin/sh
import glob
from pathlib import Path
import os

files = Path("./").rglob("*")

for f in files:

    if ".git" in str(f):
        continue
    if ".js" in str(f):
        continue
    if "manage.py" in str(f):
        continue

    if os.path.isdir(f):
        continue

    d = str(os.system(f"git check-ignore {f} > /dev/null"))
    if d == "0":
        continue

    try:
        spdx_str = "# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)\n#\n# SPDX-License-Identifier: MIT\n\n"
        with open(f, "r") as original:
            data = original.read()

        if "SPDX" in data:
            data = data.replace("2022 German", "2022 German")
            print(f)
            with open(f, "w") as modified:
                modified.write(data)
    except (UnicodeDecodeError, PermissionError):
        continue
