# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from pyftdi.eeprom import FtdiEeprom

eeprom = FtdiEeprom()
eeprom.open("ftdi://ftdi:232h/1")
with open("backup.eeprom", "w+") as f:
    eeprom.dump_config(f)
eeprom.set_manufacturer_name("DLR")
eeprom.set_product_name("MMS")
eeprom.set_serial_number("Links")
eeprom.set_property("power_max", "450")
eeprom.commit(dry_run=False)
eeprom.close()
