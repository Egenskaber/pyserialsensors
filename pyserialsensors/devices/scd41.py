# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Sensor Class for SCD41

Overview
https://sensirion.com/de/produkte/katalog/SCD41/

Datasheet
https://sensirion.com/media/documents/E0F04247/631EF271/CD_DS_SCD40_SCD41_Datasheet_D1.pdf

"""

import time
from ..core.error import Error
from ..core.sensor import I2CSensor


class SCD41(I2CSensor):
    """
    SCD41 - CO2 and RH/T Sensor Module
    """

    __name__ = "SCD41"
    _serial_mode = "I2C"
    _units = {
        "co2": "ppm",
        "temperature": "C",
        "humidity": "pct",
    }

    _SENSOR_ADDRESS = 0x62
    _crc_check_init = 0xFF
    _clk_stretch = True
    _i2c_freq = 1e3
    measurement_interval = 5

    """
    Basic commands. Signal update interval 5 sec.
    """
    # Start/Read/Stop periodic measurement
    __cmd_start_per_meas = [0x21, 0xB1]  # noqa
    __cmd_read_meas = [0xEC, 0x05]  # noqa
    __cmd_stop_per_meas = [0x3F, 0x86]  # noqa

    """
    On-Chip output signal compensation
    """
    # Set/Get temperature offset
    # Set temperature offset = (TempOffset in °C) * 2^16 / 175
    __cmd_set_temperature_offset = [0x24, 0x1D]  # noqa
    __cmd_get_temperature_offset = [0x23, 0x18]  # noqa

    # Set/Get sensor altitude
    # Set altitude = sensor altitude in m
    __cmd_set_altitude = [0x24, 0x27]  # noqa
    __cmd_get_altitude = [0x23, 0x22]  # noqa

    # Set ambient pressure
    # Set ambient pressure = ambient pressure in Pa / 100
    __cmd_set_ambient_pressure = [0xE0, 0x00]  # noqa

    """
    Field calibration
    """
    # Perform forced recalibration
    # Set target concentration = target concentration in ppm CO2
    __cmd_forced_calibration = [0x36, 0x2F]  # noqa

    # Set/Get automatic self calibration (default = enabled)
    # Enable = 1
    # Disable = 0
    __cmd_asc_set_state = [0x24, 0x16]  # noqa
    __cmd_asc_set_enable = [0x24, 0x16, 0x00, 0x01]  # noqa
    __cmd_asc_set_disable = [0x24, 0x16, 0x00, 0x00]  # noqa
    __cmd_asc_get_state = [0x23, 0x13]  # noqa

    """
    Low power operation for use with constrained power budget. Signal update interval 30 sec.
    """
    # Start low power periodic measurement
    __cmd_start_lowpower_per_meas = [0x21, 0xAC]  # noqa

    # Get data ready status in low power operation
    __cmd_get_data_rdy = [0xE4, 0xB8]  # noqa

    # Measure low power single shot
    __cmd_measure_single_shot = [0x21, 0x9D]  # noqa

    # Measure low power single shot of relative humidity and temperature only
    __cmd_measure_single_shot_rht_only = [0x21, 0x96]  # noqa

    # Power down sensor
    __cmd_power_down = [0x36, 0xE0]  # noqa

    # Wake up sensor
    __cmd_wake_up = [0x36, 0xF6]  # noqa

    """
    Advanced features
    """
    # Store persistent settings
    __cmd_persist_settings = [0x36, 0x15]  # noqa

    # Get serial number
    __cmd_get_serial_number = [0x36, 0x82]  # noqa

    # Perform self test
    __cmd_perform_self_test = [0x36, 0x39]  # noqa

    # Perform factory reset
    __cmd_perform_factory_reset = [0x36, 0x32]  # noqa

    # Perform reinitialization
    __cmd_perform_reinit = [0x36, 0x46]  # noqa

    def __init__(self, *args, **kwargs):
        """"""

        # Identifiers
        super().__init__(*args, **kwargs)
        self._logger.debug("ALARM")
        self.serial_number = None
        self.exists = self.sensor_exists()

    def txrx(self, *args, crc=True, **kwargs):
        """
        Standard txrx module is extended by a crc check
        """
        data = super().txrx(*args, **kwargs)
        if data is not None:
            if crc and False in self.crc_check(data):
                self._logger.warning("CRC check failed.")
                data = None
        return data

    def sensor_exists(self):
        """
        Test if sensor is plugged in and works proper

        Returns:
            bool: True if test was successful otherwise False

        """
        self.stop_measurement()
        serial_number = self.get_serial_number()

        exists = False

        if isinstance(serial_number, str) and serial_number != "":
            self.serial_number = serial_number
            exists = True
        else:
            self.error = "No connection."
        return exists

    def get_asc_status(self):
        """
        Get automatic self-calibration (acs) status.

        Returns:
            bool: True if asc active, false otherwise.

        """
        data = self.txrx(self.__cmd_asc_get_state, readlen=3)

        if data is not None:
            if data[1] == 1:
                return True
            elif data[1] == 0:
                return False
            else:
                return None

    def stop_measurement(self):
        """Stop measurement run."""
        self.txrx(self.__cmd_stop_per_meas, readlen=0)

    def reset(self):
        """
        Reinitializing the sensor
        """
        self.txrx(self.__cmd_perform_reinit, readlen=0)
        time.sleep(0.5)

    def crc_check(self, ba):
        """
        Assert if received data is valid

        Args:
            ba (bytearray): Byte array with a crc check sum at every third entry

        Returns:
            bytearray: Array of booleans indicating correct crc check sums

        """
        crc = [False] * int(len(ba) / 3)
        if len(ba) % 3 != 0:
            return crc

        for i in range(0, len(ba), 3):
            crc[int(i / 3)] = Error.checksum(
                byte_values=[ba[i], ba[i + 1]],
                crc_value=ba[i + 2],
                crc_init=self._crc_check_init,
            )[1]
        return crc

    def get_serial_number(self):
        """
        Get device serial number

        Returns:
            str: serial number or bool if no number has been read

        """
        for i in range(self._max_attempts):
            data = self.txrx(self.__cmd_get_serial_number, crc=False, readlen=9)

            if data is not None:
                self.serial_number = ""
                for i in [0, 1, 3, 4, 6, 7]:
                    self.serial_number += str(data[i])

                return self.serial_number
            else:
                self.error = Error().crc(self)
                msg = "Failed to fetch serial number."
                msg += f"(attempt  {i}/{self._max_attempts})"
                self._logger.debug(msg)
        return False

    def set_asc(self, state):
        """
        Setup automatic self calibration

        Args:
            state (bool): activate or deactivate automatic self-calibration

        """
        current_state = self.get_asc_status()
        if current_state != state:
            if state:
                self.txrx(self.__cmd_asc_set_enable, readlen=0)
            else:
                self.txrx(self.__cmd_asc_set_disable, readlen=0)

        time.sleep(self.measurement_interval + 1)

    def prepare_measurement(self, amb_pressure=0):
        t0 = time.time()
        self.start_periodic_measurement(amb_pressure=amb_pressure)
        self._logger.debug("Measurement prep. took %d seconds.", int(time.time() - t0))

    def start_periodic_measurement(self, amb_pressure: int = 0):
        """
        Initializes a periodic measurement of CO2 temperature and humidity

        Args:
            amb_pressure (int): ambiet pressure (default==off) ranges from 700-1400mBar

        Returns:
            bool: Periodic measurement established successfully (true) or failed (false)

        """
        self.set_asc(True)

        assert amb_pressure == 0 or amb_pressure > 700, "amb_pressure too low"
        assert amb_pressure == 0 or amb_pressure < 1400, "amb_pressure too high"

        amb_pressure = 1000
        if amb_pressure == 0:
            # defaults to 1013.25mBar
            p_lsb = 0x00
            p_msb = 0x00
            p_crc = 0x81

        else:
            p_msb, p_lsb = divmod(amb_pressure, 256)
            p_crc = Error.checksum(
                byte_values=[p_msb, p_lsb],
                crc_value=0x00,
                crc_init=self._crc_check_init,
            )[0]
        cmd = self.__cmd_start_per_meas + [p_msb, p_lsb, p_crc]
        self.txrx(cmd, readlen=0)
        time.sleep(1)

    def get_data_rdy(self):
        """
        Check if measurement data is available

        Returns:
            bool: True if data is present, false otherwise.

        """
        data = self.txrx(self.__cmd_get_data_rdy, readlen=3)
        if data is None:
            return False

        data = self.bytes_to_u16(data[0], data[1])

        if data is not None and data != 32768:
            return True
        self._logger.debug("Data is not ready.")
        return False

    def get_data(self):
        """
        Read CO2 concentration, temperature and humidty

        Returns:
            dict: data dictionary (keys: error, values)

        """

        # Wait until data is ready
        i = 0
        self.data = self.default_data()

        while not self.get_data_rdy():
            i += 1
            time.sleep(2 * self.measurement_interval / (self._max_attempts - 1))
            if i > self._max_attempts:
                self._logger.warning("Reset.")
                self.error = Error().read(self)
                self.reset()
                self.prepare_measurement()
                return self.error

        ba = self.txrx(self.__cmd_read_meas, readlen=9)
        if ba is not None:
            co2 = self.bytes_to_u16(ba[0], ba[1])
            T = self.bytes_to_u16(ba[3], ba[4])
            T = -45 + (175 * (T / 65535))
            RH = self.bytes_to_u16(ba[6], ba[7])
            RH = 100 * (RH / 65535)
            self.data["error"] = False
            self.data["values"] = {}
            self.data["values"]["co2"] = {"value": co2, "unit": self._units["co2"]}

            self.data["values"]["temperature"] = {
                "value": T,
                "unit": self._units["temperature"],
            }
            self.data["values"]["humidity"] = {
                "value": RH,
                "unit": self._units["humidity"],
            }
            return self.data
        else:
            self.error = Error().read(self)
            self.prepare_measurement()
            return self.error
