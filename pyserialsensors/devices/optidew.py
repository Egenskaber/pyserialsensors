# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT
# https://unix.stackexchange.com/questions/67936/attaching-usb-serial-device-with-custom-pid-to-ttyusb0-on-embedded

import serial
import time
from ..core.sensor import UARTSensor
from serial.tools import list_ports
import timeout_decorator
import logging

# logging.basicConfig(level=logging.DEBUG)


class Optidew(UARTSensor):
    __name__ = "Optidew"
    _serial_mode = "COM"
    __supported_sensors = ["Optidew"]
    _units = {"dewpoint": "°C", "temperature": "°C", "humidity": "pct", "state": "-"}
    _max_waiting_time = 0.5  # [s]
    __max_attempts = 10

    def __init__(self, *args, scan=False, **kwargs):
        super().__init__(*args, loglvl=logging.INFO, **kwargs)

        # Set serial parameters (see manual appendix B)
        self.dev = self.connect()

        if not scan:
            self._logger.name = str(self)
            self.serial_number = self.get_serial_number()
            self.ftdi_serial = self.get_ftdi_serial()

    def connect(self) -> serial.Serial:
        """
        Initiate bus connection.
        """
        bus = serial.Serial(
            port=self.port,
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1,
        )

        if bus.is_open:
            bus.close()
        bus.open()

        return bus

    def reconnect(self) -> None:
        port = self.find_port(self.ftdi_serial)
        if port is None:
            return None
        self.port = port
        self.dev = self.connect()

    def get_ftdi_serial(self) -> str:
        """ Read FTDI Serial to USB converter serial number """
        ftdi_serial = "undefined"
        all_comports = list_ports.comports()
        for cp in all_comports:
            if cp.device == self.port:
                ftdi_serial = cp.serial_number
                break
        ftdi_serial = ftdi_serial
        return ftdi_serial

    def sensor_exists(self) -> str:
        """
        Check if a sensor is present by reading the serial number
        """
        try:
            serial_number = self.get_serial_number()
            if serial_number == "undefined":
                return None
        except StopIteration:
            if self._logger is not None:
                self._logger.info("Stop iteration.")
            return None

        if self._logger is not None:
            self._logger.info(f"Sensor serial: {serial_number}")

        return serial_number

    def prepare_measurement(self) -> None:
        self._log = logging.getLogger(name=str(self))
        self.serial_number = self.get_serial_number()
        self.ftdi_serial = self.get_ftdi_serial()

    def start(self):
        pass

    def stop(self):
        pass

    def get_data(self) -> dict:
        data = self.read_values()
        if data is None:
            if self._log is not None:
                self._log.error("Error fetching values.")
        time.sleep(1)
        return data

    def getTdp(self, value: int) -> float:
        """ Convert value to dewpoint temperature (Unit preset) """
        if value < 8000:
            Tdp = value * 0.1
        else:
            Tdp = (8000 - value) * 0.1
        self._logger.debug("Tdp conversion %d %0.2f", value, Tdp)
        return round(Tdp, 2)

    def getT(self, value: int) -> float:
        """ Convert value to temperature (Unit preset) """
        if value < 8000:
            T = value * 0.1
        else:
            T = (8000 - value) * 0.1
        T = round(T, 2)
        if T == 700.0:
            T = None
        self._logger.debug("T conversion %d %s", value, str(T))
        return T

    def getRH(self, value: int) -> float:
        rh = value / 10
        self._logger.debug("RH conversion %d %0.2f", value, rh)
        return rh

    def read_values(self) -> dict:
        """
        Collect measurement data
        """

        data = self.default_data()
        data["values"] = {}
        for key in self._units.keys():
            data["values"][key] = {"value": None, "unit": self._units[key]}

        raw = self.txrx("y")

        if len(raw) == 0:
            return data

        if raw[0] != ":":
            self._logger.debug("Invalid output string.")
            return data

        state = int(raw[21])
        Tdp = self.getTdp(int(raw[1:5]))
        T = self.getT(int(raw[5:9]))

        if T is None:
            RH = None
        else:
            RH = self.getRH(int(raw[9:13]))

        data["values"]["dewpoint"]["value"] = Tdp
        data["values"]["humidity"]["value"] = RH
        data["values"]["temperature"]["value"] = T
        data["error"] = False
        self.data = data
        return data

    @timeout_decorator.timeout(10, timeout_exception=StopIteration)
    def get_serial_number(self) -> str:
        return self.get_firmware_version()

    def __str__(self):
        return f"Optidew@{self.port}"

    def get_firmware_version(self) -> str:
        answ = self.txrx("st")
        time.sleep(1)
        for i in range(3):
            answ = self.txrx("ver")
            if len(answ) > 0 and "_" in answ:
                self._logger.info("Firmware %s", answ.rstrip())
                return answ.rstrip()
        return ""

    def txrx(self, cmd, attempt=0) -> bytearray:
        if attempt > self.__max_attempts:
            return ""
        self.write(cmd)
        answ = self.read().decode("utf-8")

        if len(answ) == 0 or "nvalid" in answ:
            self.txrx(cmd, attempt=attempt + 1)
        return answ

    def write(self, cmd: str) -> str:
        """ Sends a command to the serial device """
        self._logger.debug("-> send " + cmd)
        cmd = cmd + "\r"
        self.dev.write(cmd.encode())
        return cmd

    def read(self, max_attempts=3) -> str:
        """Reads a line from Optidew."""
        i = 0
        while i < max_attempts:
            try:
                data = self.dev.readline()
            except serial.serialutil.SerialException:
                data = b""
            self._logger.debug("<- got " + data.decode("utf-8"))
            if data != b"":
                break
            i += 1
        return data


if __name__ == "__main__":
    dev = Optidew("/dev/ttyUSB0", loglvl=logging.INFO)
    while True:
        data = dev.read_values()
        if not data["error"]:
            print(data["values"]["dewpoint"]["value"])
        else:
            print("err")
