Supported Hardware
=================================

All Sensors
---------------------------------

* BME280
* SHT85
* NAU7802 / Qwiic Scale
* ADS1015
* MAX31865
* Sensirion SFM familiy
* Sensirion SDP familiy
* SCD30
* SPS30
* SMGAir 2
* S8000
* Optidew

Tested with
---------------------------------

* BME280
* SHT85
* Sensirion SFM familiy
* NAU7802 / Qwiic Scale
* ADS1015
* SDP810
* MAX31865
* S8000
* SCD30
* SPS30
* SMGAir 2
* Optidew
