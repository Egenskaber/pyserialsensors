.. include:: ../defs.rst

.. module :: pyserialsensors.devices.scd30

SCD30 - C02 Sensor
==============================================

.. autoclass :: SCD30
 :members:
