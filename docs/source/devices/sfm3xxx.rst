.. include:: ../defs.rst

.. module :: pyserialsensors.devices.sfm3xxx

SFM volume flow sensors
==============================================

.. autoclass :: SFM
 :members:

.. autoclass :: SFMXXX
 :members:

.. autoclass :: SFM3XXX
 :members:
