.. include:: ../defs.rst

.. module :: pyserialsensors.devices.shtxx

SHT85 - Temperature and humidity sensor
==============================================

.. autoclass :: SHT85
 :members:
