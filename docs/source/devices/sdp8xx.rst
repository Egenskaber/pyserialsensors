.. include:: ../defs.rst

.. module :: pyserialsensors.devices.sdp8xx

SDP8XX - Differential pressure sensor
==============================================

.. autoclass :: SDP8
 :members:
