Welcome pyserialsensors's documentation!
==============================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   hardware
   devices
   installation
   license
   authors
   contributing

.. include:: readme.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
